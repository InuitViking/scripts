#!/bin/bash
# This is for lynis 1.1.7 or similar, so this one is outdated and wont work with newer versions of lynis.
# 1. The script runs "lynis -c -Q --no-colors" and put the output in a temporary file in /tmp
# 2. Thereafter, grep is run to check if the keyword "WARNING" exists in that temporary file and outputs into a different temporary file.
# 3. The exit code of the grep command is stored in a variable for later check.
# 4. The content of the two previous files are put in two variables.
# 5. It then removes the temporary files.
# 6. If the exit code of grep is 1, then echo a message that no warnings were found
# 7. If the exit code of grep is 0, then echo a full report, wihh the warnings on top.
# 8. If neither, echo a message about that it's neither 0 nor 1 and then show the actual exit code (of grep).

# Variables
text1="These are the warnings I found:\n\n"
text2="\n\n\n#*#*#*#**#*#*#*# \n\n Check in /var/log/lynis.log or look below for more information: \n\n #*#*#*#**#*#*#*#\n\n\n"

# Run lynis and put the output in a file (to control where we want to display the output)
/usr/sbin/lynis audit system -Q --no-colors > /tmp/lynis_full.tmp
# grep for warnings and put the output in a different file
grep /tmp/lynis_full.tmp -e"WARNING" > /tmp/lynis_short.tmp
# Put grep exit code in a variable.
grep_exit_code=$?

# Put the content of the files in variables
lynis_full=$(cat /tmp/lynis_full.tmp)
lynis_short=$(cat /tmp/lynis_short.tmp)

# Remove temporary files
rm -rf /tmp/lynis_full.tmp
rm -rf /tmp/lynis_short.tmp

# If exit code is 1
if [ $grep_exit_code -eq 1 ]; then
  # Echo a message 
  echo "I found no warnings."
# If exit code is 0
elif [ $grep_exit_code -eq 0 ]; then
  # Echo a compiled report
  echo -e "$text1 $lynis_short $text2 $lynis_full"
# If neither
else
  # Echo a different message
  echo "Exit code not 0 nor 1; exit code: $grep_exit_code"
fi
