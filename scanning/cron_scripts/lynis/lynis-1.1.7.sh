#!/bin/bash
# This is for lynis 1.1.7 or similar, so this one is outdated and wont work with newer versions of lynis.
# This script is meant to be used in cron jobs.
# What it does:
# 1. The script runs "lynis -c -Q --no-colors" and puts the output in a temporary file in /tmp.
# 2. Therafter, grep is run to check if the keyword "WARNING" exists in the output of the previously run lynis command, and the output is stored in a (different) file in /tmp.
# 3. The exit code of the grep command is stored in a variable for later check.
# 4. The content of the two previous mentioned files are put in two variables
# 5. It then removes the temporary files created earlier.
# 6. It then checks if the grep exit code was 0, and if so, echo a report.

# Variables
text1="These are the warnings I found: \n\n"
text2="\n\n\n#*#*#*#**#*#*#*# \n \n Check in /var/log/lynis.log or look below for more information: \n \n #*#*#*#**#*#*#*#\n\n\n"

# Run lynis and put the output in a file (to control where we want to display the output.)
/usr/sbin/lynis -c -Q --no-colors > /tmp/lynis_full.tmp
# grep for warnings and put the output in a different file
grep /tmp/lynis_full.tmp -e"WARNING" > /tmp/lynis_short.tmp
# Check grep exit code. 0 is bad and 1 is good.
grep_exit_code=$?

# Put the content of the files in variables.
lynis_short=$(cat /tmp/lynis_short.tmp)
lynis_full=$(cat /tmp/lynis_full.tmp)

# Remove temporary files, because we don't need it anymore
rm -rf /tmp/lynis_short.tmp
rm -rf /tmp/lynis_full.tmp

# If grep exit code is 0
if [ $grep_exit_code -eq 0 ]; then
  # Then echo the found warnings and thereafter the full report.
  echo -e "$text1 $lynis_short $text2 $lynis_full"
fi
