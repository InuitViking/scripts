#!/bin/bash
# This bad boy is meant to be run as a cronjob.
# It's actually not recommended to ignore false positives, but in a cronjob, getting mail about
# a false positive every day is annoying, so here you go.
# "ignore-fp" will be updated as time goes, as there will be found more false positives.

# Create text variables
text0="These are without false positives, but remember to run chkrootkit without filtering.\n"
text1="Anyway, I found something!:\n\n"
text2="\n\n\nThis is run with a filter to not clutter your inbox with false positives.\nPlease run chkrootkit as-is to find more potential suspicious files."

# Run chkrootkit
/usr/sbin/chkrootkit > /tmp/chkrootkit_full.tmp
# Grep any non-matching lines (things NOT found in ignore-fp) from the previous temporary file and output that in a new temporary file.
/bin/grep -v --file=/usr/local/share/chkrootkit/ignore-fp /tmp/chkrootkit_full.tmp > /tmp/chkrootkit_short.tmp

# Check if short file is empty or not
[ -s /tmp/chkrootkit_short.tmp ]
# Put exit code in variable
exit_code=$?

# Put the contents of file in a variable
chk_short=$(cat /tmp/chkrootkit_short.tmp)

# Delete temporary files
rm -rf /tmp/chkrootkit_full.tmp
rm -rf /tmp/chkrootkit_short.tmp

# If the file is NOT empty
if [ $exit_code -eq 0 ]; then
    echo -e "$text0 $text1 $chk_short $text2"
fi
