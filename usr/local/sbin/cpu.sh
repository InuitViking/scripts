#!/bin/bash

# Function that converts floats to nearest int
FloatToInt() {
	printf "%.0f\n" "$@"
}

# Define discord bath
discord="/opt/discord.sh/discord.sh"

# Calculate the load in percent
LOAD=$(top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1}')
# Convert to int
LOAD=$(FloatToInt "${LOAD}")
# Define threshold
MAXLOAD=$1
# Variables
CPUSTATUSPATH="/tmp/cpustatus"
CPUSTATUS=$(cat "${CPUSTATUSPATH}")
IP=$(hostname -I)

Average() {
	SUM=0
	NUMBERS=0

	for a in {1..10}; do
		b="${LOAD}"
		for j in $b; do
			[[ $j =~ ^[0-9]+$ ]] || { echo "$j is not a number" >&2 && exit 1; }
			((NUMBERS+=1)) && ((SUM+=j))
		done
		sleep 1
	done

	((NUMBERS==0)) && avg=0 || avg=$(echo "$SUM / $NUMBERS" | bc -l)
	echo "${avg}";
}

AVERAGE=$(Average)
AVERAGE=$(FloatToInt "${AVERAGE}")

# If load is greater than max load
if [ "${AVERAGE}" -gt "${MAXLOAD}" ]; then

	# Get a list of processes and save them to a file
	ps -eo %mem,pid,user,args >/tmp/processes.txt

	# If cpu status is ok
	if [[ "${CPUSTATUS}" == "ok" ]]; then
		# Send to discord about the problem
		$discord --title "CRITICAL"\
			--color "0xFF0000"\
			--field "Problem on ${HOSTNAME} ; Average CPU load in the past ten seconds is **${LOAD}%** ; false"\
			--field "Threshold ; ${MAXLOAD}% ; false"\
			--field "Information ; A list of processes run at this time has been saved to /tmp/processes.txt ; false"\
			--footer "IPs: ${IP}"\
			--timestamp

		# Write status to status file
		echo "critical" > "${CPUSTATUSPATH}"
	fi

	# Write problem to stderr
	echo >&2 "## CPU load is ${LOAD}%! That is greater than the set allowed maximum of ${MAXLOAD}% ##"
# If all is okay
else

	# if status is critical
	if [[ "${CPUSTATUS}" == "critical" ]]; then

		# Write to discord about the status
		$discord --title "RESOLVED"\
			--color "0x00FF00"\
			--field "Problem resolved on ${HOSTNAME} ; Average CPU load in the past ten seconds is ${LOAD}% - all is good. ; false"\
			--field "Threshold ; ${MAXLOAD}% ; false"\
			--footer "IPs: ${IP}"\
			--timestamp

		# Write ok to status file
		echo "ok" > "${CPUSTATUSPATH}"
	fi

	# Write status to stdout
	echo "CPU load is ${LOAD}% which is below the allowed maximum of ${MAXLOAD}%. Everything is okay."
fi
