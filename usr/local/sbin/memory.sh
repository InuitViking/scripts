#!/bin/bash

discord="/opt/discord.sh/discord.sh"

FREE=$(free -mt | grep Mem | awk '{print $4}')
MINFREE=$1
MEMORYSTATUSPATH="/tmp/memorystatus"
MEMORYSTATUS=$(cat "${MEMORYSTATUSPATH}")
IP=$(hostname -I)

# If free is lower than minfree
if [ "${FREE}" -lt "${MINFREE}" ]; then

	# Get a list of processes and save to file
	ps -eo %mem,pid,user,args >/tmp/processes.txt

	# If memory status is ok
	if [[ "${MEMORYSTATUS}" == "ok" ]]; then

		# Report error to discord
		$discord --title "CRITICAL"\
			--color "0xFF0000"\
			--field "Problem on ${HOSTNAME} ; Free memory is **${FREE}MB** ; false"\
			--field "Threshold ; ${MINFREE}MB ; false"\
			--field "Information ; A list of processes run at this time has been saved to /tmp/processes.txt ; false"\
			--footer "IP: ${IP}"\
			--timestamp

		# Update status file to critical
		echo "critical" > "${MEMORYSTATUSPATH}"
	fi

	# Echo error to stderr
	echo >&2 "## Free memory is ${FREE}! That is lower than the set allowed minimum of ${MINFREE} ##"

# If all is ok
else
	# If status is critical
	if [[ "${MEMORYSTATUS}" == "critical" ]]; then

		# Write message to discord
		$discord --title "RESOLVED"\
			--color "0x00FF00"\
			--field "Problem resolved on ${HOSTNAME} ; Free memory is ${FREE}MB - all is good. ; false"\
			--field "Threshold ; ${MINFREE}MB ; false"\
			--footer "IP: ${IP}"\
			--timestamp

		# Write ok to status file
		echo "ok" > "${MEMORYSTATUSPATH}"
	fi

	# Write to message to stdout
	echo "Free memory is ${FREE} which is above the allowed minimum of ${MINFREE}. Everything is okay."
fi
