#!/bin/bash

discord="/opt/discord.sh/discord.sh"

AVAILABLE=$(df -hB G /dev/vda1 | grep vda1 | column -t | awk '{ print $4 }' | sed 's/G//g')
WARNING=$1
CRITICAL=$2
DISKSTATUSPATH="/tmp/diskstatus"
DISKSTATUS=$(cat "${DISKSTATUSPATH}")
IP=$(hostname -I)
# Get these variables so we don't have to run them twice
HOME=$(du -sh /home | awk '{ print $1 }')
VAR=$(du -sh /var | awk '{ print $1 }')


# If available is low than or equal to critical
if [[ $AVAILABLE -lt $CRITICAL || $AVAILABLE -eq $CRITICAL ]]; then

	# If disk status is other than "critical"
	if [[ "${DISKSTATUS}" == "ok" || "${DISKSTATUS}" == "warning" ]]; then

		# Send message to discord and update about the new status
		$discord --title "CRITICAL"\
			--color "0xFF0000"\
			--field "Problem on ${HOSTNAME} ; Available storage is **${AVAILABLE}G** ; false"\
			--field "Critical threshold ; ${CRITICAL}G ; false"\
			--field "Information ; \/home: ${HOME}\n\/var: ${VAR} ; false"\
			--footer "IP: ${IP}"\
			--timestamp

		# Update the status file with the new status
		echo "critical" > "${DISKSTATUSPATH}"
	fi

	# Write to stderr informing of the problem.
	echo >&2 "## Available storage is ${AVAILABLE}G! That is lower or equal to the set critical minimum of ${CRITICAL}G ##"

# If available is lower than or equal to warning
elif [[ $AVAILABLE -lt $WARNING || $AVAILABLE -eq $WARNING ]]; then

	# If status is other than warning
	if [[ "${DISKSTATUS}" == "ok" || "${DISKSTATUS}" == "critical" ]]; then

		# Send to discord updating about the problem
		$discord --title "WARNING"\
			--color "0xFFFF00"\
			--field "Problem on ${HOSTNAME} ; Available storage is **${AVAILABLE}G** ; false"\
			--field "Warning threshold ; ${WARNING}G ; false"\
			--field "Information ; \/home: ${HOME}\n\/var: ${VAR} ; false"\
			--footer "IP: ${IP}"\
			--timestamp

		# Update the status file with warning
		echo "warning" > "${DISKSTATUSPATH}"

		# Write problem to stderr
		echo >&2 "## Available storage is ${AVAILABLE}G! That is lower or equal to the set warning minimum of ${WARNING}G ##"
	fi

# If status is ok
else
	# If status is other than ok
	if [[ "${DISKSTATUS}" == "critical" || "${DISKSTATUS}" == "warning" ]]; then

		# Send message to discord to update about the status
		$discord --title "RESOLVED"\
			--color "0x00FF00"\
			--field "Problem resolved on ${HOSTNAME} ; Available storage is ${AVAILABLE}G - all is good. ; false"\
			--field "Thresholds ; Warning: ${WARNING}G\nCritical: ${CRITICAL}G ; false" \
			--footer "IP: ${IP}"\
			--timestamp

		# Write ok to status file
		echo "ok" > "${DISKSTATUSPATH}"
	fi

	# Write status to stdout
	echo "Available storage is ${AVAILABLE}G which is greater than ${WARNING}G and ${CRITICAL}G. Everything is okay."
fi
